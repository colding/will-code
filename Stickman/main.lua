mouse = {}
boxleft = {}
Menu = {}
platform = {}
border = {}
top = {}
logo = {}
fire = {}

require "camera"

function love.load()
  --Assets load
  --sound
  death= love.audio.newSource("Sound/death.wav")
  buttonstart= love.audio.newSource("Sound/buttonstart.wav")
  menumusic= love.audio.newSource("Sound/menumusic.wav")
  buttonhover= love.audio.newSource("Sound/buttonhover.mp3")
  buttonclick= love.audio.newSource("Sound/buttonclick.mp3")
  
  menumusic:setVolume(0.3)
  buttonstart:setVolume(0.3)
  
  
   -- platforms
  platform.platform = love.graphics.newImage("sprites/Platform1.png")
  platform.platform2 = love.graphics.newImage("sprites/Platform2.jpg")
  platform.platform3 = love.graphics.newImage("sprites/Platform3.jpg")
  platform.platform4 = love.graphics.newImage("sprites/Platform4.png")
  platform.platform5 = love.graphics.newImage("sprites/Platform5.png")
  platform.platform6 = love.graphics.newImage("sprites/Platform6.png")
  platform.platform7 = love.graphics.newImage("sprites/Platform7.png")
  platform.platform8 = love.graphics.newImage("sprites/Platform8.png")
  platform.platform9 = love.graphics.newImage("sprites/Platform9.png")
  platform.platform10 = love.graphics.newImage("sprites/Platform10.png")
  platform.platform11 = love.graphics.newImage("sprites/Platform11.png")
  platform.platform12 = love.graphics.newImage("sprites/Platform12.png")
  platform.platform13 = love.graphics.newImage("sprites/Platform13.png")
  platform.platform14 = love.graphics.newImage("sprites/Platform14.png")
  platform.platform15 = love.graphics.newImage("sprites/Platform15.png")
  platform.platform16 = love.graphics.newImage("sprites/Platform16.png")

  platform.platformPosX = -70
  platform.platformPosY = 250
  platform.platform2posx = -20
  platform.platform2posy = 450
  platform.platform3PosX = 220
  platform.platform3PosY = 455
  platform.platform4PosX = -70
  platform.platform4PosY = 650
  platform.platform5PosX = -20
  platform.platform5PosY = 850
  platform.platform6PosX = -70
  platform.platform6PosY = 1050
  platform.platform7PosX = 130
  platform.platform7PosY = 1055
  platform.platform8PosX = -20
  platform.platform8PosY = 1255
  platform.platform9PosX = -70
  platform.platform9PosY = 1455
  platform.platform10PosX = -20
  platform.platform10PosY = 1655
  platform.platform11PosX = 250
  platform.platform11PosY = 1650
  platform.platform12PosX = -70
  platform.platform12PosY = 1855
  platform.platform13PosX = 50
  platform.platform13PosY = 2055
  platform.platform14PosX = 150
  platform.platform14PosY = 2055
  platform.platform15PosX = -100
  platform.platform15PosY = 2255
  platform.platform16PosX = 300
  platform.platform16PosY = 2455
  
  --obstacle
  
  fire.fire1 = love.graphics.newImage("sprites/fire1.png")
  fire.fire2 = love.graphics.newImage("sprites/fire2.png")

  
  fire.fire1PosX = 80
  fire.fire1PosY = 650
  
  fire.fire2PosX = 120
  fire.fire2PosY = 1255
  

 -- border
top.top = love.graphics.newImage("sprites/topBackground.png")
border.border = love.graphics.newImage("sprites/border.png")
border.border15 = love.graphics.newImage("sprites/border.png")
border.border2 = love.graphics.newImage("sprites/border.png")
border.border25 = love.graphics.newImage("sprites/border.png")
border.border3 = love.graphics.newImage("sprites/borderTop.png")

top.x = -400
top.y = -100
border.borderx = -800
border.bordery = -100

border.border15x = -800
border.border15y = 900

border.border2x = 400
border.border2y = -100

border.border25x = 400
border.border25y = 900

border.border3x = -100
border.border3y = -600

  

  
  
    
  Stickman = love.graphics.newImage("sprites/newstickman.png")

  
  
  Badguy = love.graphics.newImage("sprites/Badguy.PNG")
  
  point = love.graphics.newImage("Asset/point.jpg")


  checkpoint = love.graphics.newImage("sprites/checkpoint.jpg")

  checkpoint = love.graphics.newImage("sprites/checkpoint.jpg")
  save = love.graphics.newImage("sprites/save.jpg")
  --fire = love.graphics.newImage("sprites/Obstical.jpg")
  Moveplat = love.graphics.newImage("Asset/move.jpg")
  Menu.background= love.graphics.newImage("Asset/MenuBackground.jpg")
  Menu.backgroundQuad = love.graphics.newQuad(1,1,720/2,1280/2,720/2,1280/2)
  Menu.ButtonStart = love.graphics.newImage("Asset/MenuStart.jpg")
  Menu.ButtonStartHover = love.graphics.newImage("Asset/MenuStartHover.jpg")
  Menu.ButtonLoad = love.graphics.newImage("Asset/MenuLoad.jpg")
  Menu.ButtonLoadHover = love.graphics.newImage("Asset/MenuLoadHover.jpg")

  Menu.ButtonOptions = love.graphics.newImage("Asset/MenuOptions.jpg")
  Menu.ButtonOptionsHover = love.graphics.newImage("Asset/MenuOptionsHover.jpg")

  Menu.ButtonExit = love.graphics.newImage("Asset/MenuExit.jpg")
  Menu.ButtonExitHover = love.graphics.newImage("Asset/MenuExitHover.jpg")


  Menu.ButtonHard = love.graphics.newImage("Asset/MenuHard.jpg")
  Menu.ButtonNormal = love.graphics.newImage("Asset/MenuNormal.jpg")

  Menu.ButtonQuestion = love.graphics.newImage("Asset/MenuQuestion.jpg")
  
  Menu.ButtonLoad = love.graphics.newImage("Asset/MenuLoad.jpg")
    -- platforms
  

  --platform.platform2 = love.graphics.newImage("sprites/platform.png")


  --platform.platform2posx = 100
  --platform.platform2posy = 400
  



  --spirtes images 



  BadguyPosY = 500
  BadguyPosX = 300 
  BadguySpeed = 0.5




  --spirtes images 

  StickmanPosY = 200
  StickmanPosX = 2
  StickmanHeight = 45
  StickmanWidth = 29


-- for left and right movement
  boxleft.x = 0
  boxleft.y = 0




  -- platforms

  Stickmanground = StickmanPosY
  StickmanPosY_velocity = 0

 tempboolean = false
count = 30
  Stickmanjump = -300
  Stickmangravity = -500
  Stickmanhealth = 1






  checkpointPosY1 = 200
  checkpointPosX1 = 2
  savePosY1 = 200
  savePosX1 = 300
  pointPosY1 = 200
  pointPosX1 = -40
  
  checkpointPosY2 = 200
  checkpointPosX2 = 2
  savePosY2 = 200
  savePosX2 = 300
  pointPosY2 = 200
  pointPosX2 = -40
  
  checkpointPosY3 = 200
  checkpointPosX3 = 2
  savePosY3 = 200
  savePosX3 = 300
  pointPosY3 = 200
  pointPosX3 = -40
  
  checkpointPosY4 = 200
  checkpointPosX4 = 2
  savePosY4 = 200
  savePosX4 = 300
  pointPosY4 = 200
  pointPosX4 = -40
  
 
  pointPosY5 = 200
  pointPosX5 = -40
  
  
  pointPosY6 = 200
  pointPosX6 = -40
  
  
  pointPosY7 = 200
  pointPosX7 = -40
  
  
  pointPosY8 = 200
  pointPosX8 = -40
  
 
  pointPosY9 = 200
  pointPosX9= -40
  
  
  pointPosY10 = 200
  pointPosX10 = -40
  
 
  pointPosY11 = 200
  pointPosX11 = -40
  
  
  pointPosY12 = 200
  pointPosX12 = -40
  
  
  pointPosY13 = 200
  pointPosX13 = -40
  
  
  pointPosY14 = 200
  pointPosX14 = -40
  
  
  pointPosY15 = 200
  pointPosX15 = -40
  
 
  pointPosY16 = 200
  pointPosX16 = -40
  
  
  score = 0


  



  MoveplatPosY = 200
  MoveplatPosX = 100



  --firePosY = 200
  --firePosX = 200

  EndScene= false
  answerInput = ""
  Answer = "colding"




  Menu.StartPosY = 100


  Menu.LoadPosY = 200
  Menu.QuestionPosY = 200


  Menu.OptionsPosY = 300


  Menu.ExitPosY = 400
  
  Menu.HardPosY = 100
  Menu.NormalPosY = 200



  Menu.SaveOnePosY = 100

  menu = true
  options = false
  loadSave = false

  pause = false

  gameScene = false

  moveplat = false

  Checksave = false
  mousehover= false
  questionMenu = false
end

function love.update(dt)


  if (gameScene == true) then


    mouse.x, mouse.y = love.mouse.getPosition()


    if love.keyboard.isDown('space') then                  


      if StickmanPosY_velocity == 0 then
        StickmanPosY_velocity = Stickmanjump    
      end
    end

    if StickmanPosY_velocity ~= 0 then                                    
      StickmanPosY = StickmanPosY + StickmanPosY_velocity * dt               
      StickmanPosY_velocity = StickmanPosY_velocity -Stickmangravity * dt 
    
  end
  
  
  
camera.x = StickmanPosX - love.graphics.getWidth() / 2

camera.y = StickmanPosY - love.graphics.getWidth() / 2

  end
end
function love.draw()
--draw



  
  game_screen()
  camera:set()
     

  if (gameScene == true)then
     love.graphics.setBackgroundColor(255, 255, 255, 255)
   
    --Drawing Border
  
    love.graphics.draw(border.border, border.borderx, border.bordery)
    love.graphics.draw(border.border2, border.border2x, border.border2y)
    love.graphics.draw(border.border3, border.border3x, border.border3y)
    
    love.graphics.draw(border.border15, border.border15x, border.border15y)
    love.graphics.draw(border.border25, border.border25x, border.border25y)
    love.graphics.draw(top.top, top.x, top.y)
 

    
    --Drawing Platforms
    love.graphics.draw(platform.platform, platform.platformPosX, platform.platformPosY)
    love.graphics.draw(save, savePosX1, savePosY1)
    love.graphics.draw(checkpoint, checkpointPosX1, checkpointPosY1)
    love.graphics.draw(point, pointPosX1, pointPosY1)
    
    love.graphics.draw(platform.platform3, platform.platform3PosX, platform.platform3PosY)
    love.graphics.draw(save, savePosX2, savePosY2)
    love.graphics.draw(checkpoint, checkpointPosX2, checkpointPosY2)
    love.graphics.draw(point, pointPosX2, pointPosY2)
    
    
    
    love.graphics.draw(platform.platform2, platform.platform2posx, platform.platform2posy)
    love.graphics.draw(save, savePosX3, savePosY3)
    love.graphics.draw(checkpoint, checkpointPosX3, checkpointPosY3)
     love.graphics.draw(point, pointPosX3, pointPosY3)
     
    love.graphics.draw(platform.platform4, platform.platform4PosX, platform.platform4PosY)
    love.graphics.draw(save, savePosX4, savePosY4)
    love.graphics.draw(checkpoint, checkpointPosX4, checkpointPosY4)
     love.graphics.draw(point, pointPosX4, pointPosY4)
    
    love.graphics.draw(platform.platform5, platform.platform5PosX, platform.platform5PosY)
    
     love.graphics.draw(point, pointPosX5, pointPosY5)
    
    love.graphics.draw(platform.platform6, platform.platform6PosX, platform.platform6PosY)
    
     love.graphics.draw(point, pointPosX6, pointPosY6)
    
    love.graphics.draw(platform.platform7, platform.platform7PosX, platform.platform7PosY)
    
     love.graphics.draw(point, pointPosX7, pointPosY7)
    
    love.graphics.draw(platform.platform8, platform.platform8PosX, platform.platform8PosY)
  
     love.graphics.draw(point, pointPosX8, pointPosY8)
    
    love.graphics.draw(platform.platform9, platform.platform9PosX, platform.platform9PosY)
   
     love.graphics.draw(point, pointPosX9, pointPosY9)
    
    love.graphics.draw(platform.platform10, platform.platform10PosX, platform.platform10PosY)
 
     love.graphics.draw(point, pointPosX10, pointPosY10)
    
    love.graphics.draw(platform.platform11, platform.platform11PosX, platform.platform11PosY)
   
     love.graphics.draw(point, pointPosX11, pointPosY11)
    
    love.graphics.draw(platform.platform12, platform.platform12PosX, platform.platform12PosY)
    
     love.graphics.draw(point, pointPosX12, pointPosY12)
    
    love.graphics.draw(platform.platform13, platform.platform13PosX, platform.platform13PosY)
 
     love.graphics.draw(point, pointPosX13, pointPosY13)
    
    love.graphics.draw(platform.platform14, platform.platform14PosX, platform.platform14PosY)
  
     love.graphics.draw(point, pointPosX14, pointPosY14)
    
    love.graphics.draw(platform.platform15, platform.platform15PosX, platform.platform15PosY)
  
     love.graphics.draw(point, pointPosX15, pointPosY15)
    
    love.graphics.draw(platform.platform16, platform.platform16PosX, platform.platform16PosY)
 
     love.graphics.draw(point, pointPosX16, pointPosY16)
    
    
    love.graphics.draw(fire.fire1, fire.fire1PosX, fire.fire1PosY)
    love.graphics.draw(fire.fire2, fire.fire2PosX, fire.fire2PosY)

   
    -- Drawing Power Ups
    --love.graphics.draw(Moveplat, MoveplatPosX, MoveplatPosY)
   
    --love.graphics.draw(fire, firePosX, firePosY)
    love.graphics.draw(Badguy, BadguyPosX, BadguyPosY)
   
 
        --Drawing Player
        love.graphics.draw(Stickman, StickmanPosX, StickmanPosY)
        
        
        
coloredtext = {
    {255,1,1},
    "Health: "..Stickmanhealth.." Score: "..score
  }
  

  --love.graphics.print("Mouse Coordinates: " .. mouse.x .. ", " .. mouse.y)
  love.graphics.print(coloredtext,StickmanPosX - 180, StickmanPosY - 180)
  
  

  
  
  
  end
   camera:unset()

end

function game_screen()

  if (gameScene == true)then
    menumusic:stop()
   

    --Bad guy movemnt 
    if (BadguyPosX >=StickmanPosX)then
      BadguyPosX = BadguyPosX - BadguySpeed
    else
      BadguyPosX = BadguyPosX + BadguySpeed
    end

    if (BadguyPosY >=StickmanPosY)then
     BadguyPosY = BadguyPosY - BadguySpeed
    else
      BadguyPosY = BadguyPosY + BadguySpeed
    end
    --Badguy Collison 
   
 
   BadguyCollision = CheckCollision(StickmanPosX, StickmanPosY, StickmanWidth, StickmanHeight, BadguyPosX, BadguyPosY, 45, 45)
    if (BadguyCollision)then
      StickmanPosY =checkpointPosY1
      StickmanPosX =checkpointPosX1
      Stickmanhealth = Stickmanhealth -1
      death:play()
    end

    if (Stickmanhealth <0)then
      gameScene = false

      menu = true

      StickmanPosY =checkpointPosY1
      StickmanPosX =checkpointPosX1
      Stickmanhealth = 1
      BadguyPosY = 500
      BadguyPosX = 300 
      savePosY1 = savePosY1 - 100000000
      
      death:play()
      
      
    end



if (StickmanPosX == temp)then
  
    Stickman = love.graphics.newImage("sprites/newstickman.png")
 
      
  
  
end



    --StickmanPosX = StickmanPosX + 1.5
    hitTest = CheckCollision(boxleft.x, boxleft.y, 200, 1280, mouse.x, mouse.y, 100, 100)
    if (hitTest) then
      StickmanPosX = StickmanPosX - 1.5
      
      if (tempboolean == false)then
    
          Stickman = love.graphics.newImage("sprites/newstickmanrunalt.png")
         
      else
          Stickman = love.graphics.newImage("sprites/newstickmanrunBackward.png")
          
    
      end
      
      count = count - 1
      if count == 0 and tempboolean == false then
        tempboolean = true
        count = 30
      elseif count == 0 and tempboolean == true then 
        tempboolean = false
        count = 30
      end
      
     
    else
      StickmanPosX = StickmanPosX + 1.5
      
      if (tempboolean == false)then
    
          Stickman = love.graphics.newImage("sprites/newstickmanrun.png")
         
      else
          Stickman = love.graphics.newImage("sprites/newstickmanrunalt.png")
          
    
      end
      
      count = count - 1
      if count == 0 and tempboolean == false then
        tempboolean = true
        count = 30
      elseif count == 0 and tempboolean == true then 
        tempboolean = false
        count = 30
      end
    end
   
    hitTestb = CheckCollision(border.borderx ,border.bordery ,719, 1331, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    if (hitTestb) then
     StickmanPosX = StickmanPosX + 1.5
   end
   
       hitTest15 = CheckCollision(border.border15x ,border.border15y ,719, 1331, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    if (hitTest15) then
     StickmanPosX = StickmanPosX + 1.5
   end
     

   hitTestb2 = CheckCollision(border.border2x ,border.border2y ,719, 1331,StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    if (hitTestb2) then
     StickmanPosX = StickmanPosX - 1.5
   end
   
      hitTestb25 = CheckCollision(border.border25x ,border.border25y ,719, 1331,StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    if (hitTestb25) then
     StickmanPosX = StickmanPosX - 1.5
     end
    -- Collision for jumping on platforms
    AhitTest = CheckCollision(platform.platformPosX, platform.platformPosY, 410, 59, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest2 = CheckCollision(platform.platform2posx, platform.platform2posy, 236, 49, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest3 = CheckCollision(platform.platform3PosX, platform.platform3PosY, 179, 36, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest4 = CheckCollision(platform.platform4PosX, platform.platform4PosY, 415, 38, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest5 = CheckCollision(platform.platform5PosX, platform.platform5PosY, 418, 31, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest6 = CheckCollision(platform.platform6PosX, platform.platform6PosY, 200, 39, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest7 = CheckCollision(platform.platform7PosX, platform.platform7PosY, 213, 31, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest8 = CheckCollision(platform.platform8PosX, platform.platform8PosY, 431, 33, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest9 = CheckCollision(platform.platform9PosX, platform.platform9PosY, 385, 26, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest10 = CheckCollision(platform.platform10PosX, platform.platform10PosY, 272, 38, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest11 = CheckCollision(platform.platform11PosX, platform.platform11PosY, 164, 46, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest12 = CheckCollision(platform.platform12PosX, platform.platform12PosY, 363, 32, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest13 = CheckCollision(platform.platform13PosX, platform.platform13PosY, 92, 36, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest14 = CheckCollision(platform.platform14PosX, platform.platform14PosY, 247, 38, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest15 = CheckCollision(platform.platform15PosX, platform.platform15PosY, 428, 34, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    hitTest16 = CheckCollision(platform.platform16PosX, platform.platform16PosY, 97, 47, StickmanPosX, StickmanPosY, StickmanHeight,StickmanWidth)
    if (AhitTest) then
    StickmanPosY_velocity = 0
    else if (hitTest2) then
           StickmanPosY_velocity = 0
           else if (hitTest3) then
           StickmanPosY_velocity = 0
           else if (hitTest4) then
           StickmanPosY_velocity = 0
           else if (hitTest5) then
           StickmanPosY_velocity = 0
           else if (hitTest6) then
           StickmanPosY_velocity = 0
           else if (hitTest7) then
           StickmanPosY_velocity = 0
           else if (hitTest8) then
           StickmanPosY_velocity = 0
           else if (hitTest9) then
           StickmanPosY_velocity = 0
           else if (hitTest10) then
           StickmanPosY_velocity = 0
           else if (hitTest11) then
           StickmanPosY_velocity = 0
           else if (hitTest12) then
           StickmanPosY_velocity = 0
           else if (hitTest13) then
           StickmanPosY_velocity = 0
           else if (hitTest14) then
           StickmanPosY_velocity = 0
           else if (hitTest15) then
           StickmanPosY_velocity = 0
           else if (hitTest16) then
           StickmanPosY_velocity = 0
           
    else
      StickmanPosY = StickmanPosY + 1
                                          
     
     
    end 
  end
end
end
end
end
end
end
end
end
end
end
end
end
end
end

 

    
  end
  if (EndScene == true)then
   
    love.graphics.draw(Menu.background, Menu.backgroundQuad, 0, 0)
    love.graphics.draw(Menu.ButtonExit, 90, Menu.ExitPosY)
      coloredanswer = {
    {255,1,1},
    answerInput
  }
  coloredworked = {
    {255,1,1},
    "worked"
  }
   text = {
    {255,1,1},
    "Well done!! type answer for extra points"
  }
  coloredscore = {
    {255,1,1},
    "Score:"..score
  }

  love.graphics.print(text, 60, 100)
  love.graphics.print(coloredanswer, 60, 120)
  love.graphics.print(coloredscore, 60, 80)
  temp = answerInput
  
  if (answerInput == Answer)then
    
     love.graphics.print(coloredworked, 130,200)
     score = score + 10
   end
   
    
    
    
    end 

  if (questionMenu == true) then
    
    love.graphics.draw(Menu.background, Menu.backgroundQuad, 0, 0)
    love.graphics.draw(Menu.ButtonStart,90, Menu.StartPosY)
    
    love.graphics.draw(Menu.ButtonQuestion,90, Menu.QuestionPosY)
       
   
    
    
    end
  if (menu == true) then
      menumusic:setLooping(true)
      menumusic:play()
 
    love.graphics.draw(Menu.background, Menu.backgroundQuad, 0, 0)
    love.graphics.draw(Menu.ButtonStart,90, Menu.StartPosY)
    
    love.graphics.draw(Menu.ButtonOptions,90, Menu.OptionsPosY)
    love.graphics.draw(Menu.ButtonExit, 90, Menu.ExitPosY)
  end 
  if (loadSave == true) then
    love.graphics.draw(Menu.background, Menu.backgroundQuad, 0, 0)
    love.graphics.draw(Menu.ButtonSaveOne,90, Menu.StartPosY)
    love.graphics.draw(Menu.ButtonExit, 90, Menu.ExitPosY)

  end
  if (options == true) then
    love.graphics.draw(Menu.background, Menu.backgroundQuad, 0, 0)

    love.graphics.draw(Menu.ButtonExit, 90, Menu.ExitPosY)
    
    love.graphics.draw(Menu.ButtonHard, 90, Menu.HardPosY)

    love.graphics.draw(Menu.ButtonNormal, 90, Menu.NormalPosY)
   

    
     
  end

 
 pointCheck1 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX1,pointPosY1,27,27)
 pointCheck2 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX2,pointPosY2,27,27)
 pointCheck3 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX3,pointPosY3,27,27)
 pointCheck4 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX4,pointPosY4,27,27)
 pointCheck5 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX5,pointPosY5,27,27)
 pointCheck6 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX6,pointPosY6,27,27)
 pointCheck7 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX7,pointPosY7,27,27)
 pointCheck8 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX8,pointPosY8,27,27)
 pointCheck9 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX9,pointPosY9,27,27)
 pointCheck10 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX10,pointPosY10,27,27)
 pointCheck11 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX11,pointPosY11,27,27)
 pointCheck12 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX12,pointPosY12,27,27)
 pointCheck13 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX13,pointPosY13,27,27)
 pointCheck14 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX14,pointPosY14,27,27)
 pointCheck15 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX15,pointPosY15,27,27)
 pointCheck16 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,pointPosX16,pointPosY16,27,27)    
if (pointCheck1)then
    -- moveplat = true 
    score = score + 1
    pointPosY1 = pointPosY1 +10000000
    end
  if (pointCheck2)then
    score = score + 1
    pointPosY2 = pointPosY2 +10000000
    end
  if (pointCheck3)then
     score = score + 1
    pointPosY3 = pointPosY3 +10000000
    end
  if (pointCheck4)then
     score = score + 1
    pointPosY4 = pointPosY4 +10000000
    end
  if (pointCheck5)then
     score = score + 1
    pointPosY5 = pointPosY5 +10000000
    end
   if (pointCheck6)then
     score = score + 1
    pointPosY6 = pointPosY6 +10000000
    end
  if (pointCheck7)then
     score = score + 1
    pointPosY7= pointPosY7 +10000000
    end
  if (pointCheck8)then
     score = score + 1
    pointPosY8 = pointPosY8 +10000000
    end
  if (pointCheck9)then
     score = score + 1
    pointPosY9 = pointPosY9 +10000000
    end
  if (pointCheck10)then
     score = score + 1
    pointPosY10 = pointPosY10 +10000000
    end
  if (pointCheck11)then
     score = score + 1
    pointPosY11= pointPosY11 +10000000
    end
  if (pointCheck12)then
     score = score + 1
    pointPosY12 = pointPosY12 +10000000
    end
  if (pointCheck13)then
     score = score + 1
    pointPosY13 = pointPosY13 +10000000
    end
  if (pointCheck14)then
     score = score + 1
    pointPosY14 = pointPosY14 +10000000
    end
  if (pointCheck15)then
     score = score + 1
    pointPosY15 = pointPosY15 +10000000
    end
  if (pointCheck16)then
     score = score + 1
    pointPosY16 = pointPosY16 +10000000
end
 EndCheckpointCheck = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,platform.platform16PosX, platform.platform16PosY, 97, 47)
if (EndCheckpointCheck)then
  
  

  EndScene = true
  gameScene = false   
  
end
 
 
  moveplatCheck = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,MoveplatPosX,MoveplatPosY,27,27)

  if (moveplatCheck)then
    -- moveplat = true 

  end
  fireCheck = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,fire.fire1PosX,fire.fire1PosY,27,27)
  fireCheck2 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,fire.fire2PosX,fire.fire2PosY,27,27)
 
  if (fireCheck )then
    StickmanPosY =checkpointPosY
    StickmanPosX =checkpointPosX
   Stickmanhealth = Stickmanhealth -1
    death:play()

else if (fireCheck2)then
    StickmanPosY =checkpointPosY
    StickmanPosX =checkpointPosX
   Stickmanhealth = Stickmanhealth -1
    death:play()

end
  end

  saveCheck = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,savePosX1,savePosY1,27,27)

  saveCheck2 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,savePosX2,savePosY2,27,27)
   saveCheck3 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,savePosX3,savePosY3,27,27)
    saveCheck4 = CheckCollision(StickmanPosX,StickmanPosY,StickmanWidth, StickmanHeight,savePosX4,savePosY4,27,27)
  if (saveCheck)then
    savePosY1 = savePosY1 + 100000000
    Stickmanhealth = Stickmanhealth +1

end
  if (saveCheck2)then
    savePosY2 = savePosY2 + 100000000
    Stickmanhealth = Stickmanhealth +1
end
  if (saveCheck3)then
    savePosY3 = savePosY3 + 100000000
    Stickmanhealth = Stickmanhealth +1
end
  if (saveCheck4)then
    savePosY4 = savePosY4 + 100000000
    Stickmanhealth = Stickmanhealth +1
end

  if(StickmanPosY >= 10000000000/2) then 


    if (Stickmanhealth >= 1)then
      StickmanPosY =checkpointPosY
      StickmanPosX =checkpointPosX
      Stickmanhealth = Stickmanhealth -1
      death:play()
    else 
      gameScene = false

      menu = true
      StickmanPosY =checkpointPosY
      StickmanPosX =checkpointPosX
      Stickmanhealth = 1
      death:play()


      savePosY = savePosY - 100000000
    end
  end






  mouseX  = love.mouse.getX()
  mouseY = love.mouse.getY()
  checkStart = pointInRectangle(mouseX,mouseY,90,Menu.StartPosY,178,64)
 
  checkOptions = pointInRectangle(mouseX,mouseY,90,Menu.OptionsPosY,178,64)
  checkExit = pointInRectangle(mouseX,mouseY,90,Menu.ExitPosY,178,64)
  checkSaveOne = pointInRectangle(mouseX,mouseY,90,Menu.SaveOnePosY,178,64)
  
  checkNormal = pointInRectangle(mouseX,mouseY,90,Menu.NormalPosY,178,64)
  checkHard = pointInRectangle(mouseX,mouseY,90,Menu.HardPosY,178,64)
  if (checkNormal and options == true)then
  
    
  end
  
  if (checkHard and options == true)then
  
    
    
  end
  
  
  
  if (checkStart and menu == true)then
    love.graphics.draw(Menu.ButtonStartHover,90, Menu.StartPosY)
   
  end
  if (checkSaveOne and loadSave == true)then

    love.graphics.draw(Menu.ButtonSaveOneHover,90, Menu.SaveOnePosY)
    buttonhover:play()
  end
  
  if (checkOptions and menu == true)then
    love.graphics.draw(Menu.ButtonOptionsHover,90, Menu.OptionsPosY)
    
  end
  if (checkExit and gameScene == false )then
    love.graphics.draw(Menu.ButtonExitHover,90, Menu.ExitPosY)
    
  end
  
 
  if (gameScene == false)then
  if (mousehover==false and (checkStart or checkSaveOne or checkLoad or checkOptions or checkExit)  )then
     buttonhover:play()
     mousehover=true
  elseif (  mousehover==true and (not checkStart and not checkSaveOne and not checkLoad and  not checkOptions and  not checkExit) )then
    mousehover=false
  end
end

    

end





function Save()

end
function Physics ()

end
function love.keypressed(key)
  if (EndScene == true)then
   answerInput = answerInput..key
   end
     
end
function love.mousepressed(x, y, button, istouch)
if (questionMenu == true) then
  if (button == 1 and checkStart) then
    gameScene = true
    questionMenu = false
    
    
    end
  
  
end
if (button == 1 and checkExit and EndScene == true) then
  
  love.load()
  
end

  
  if (button == 1 and moveplat == true)then

    --check = pointInRectangle(mouseX,mouseY,platformPosX,platformPosY,178,64)
    --if (check)then
    mouseX  = love.mouse.getX()
    mouseY = love.mouse.getY()

    platform.platformPosX = mouseX
    platform.platformPosY = mouseY
    love.graphics.draw(platform.platform, platform.platformPosX, platform.platformPosY)


    --end

  end

  if ( menu == true)then
    if (button == 1 and checkSaveOne) then
      




    end
    if (button == 1 and checkStart) then
      questionMenu = true

      menu = false
     
      buttonstart:play()




      --file = love.filesystem.load("Save.txt")


      -- file:open("w")
      --file:write("Worked")


      --file:close()
      -- file:open("r")
      --data = file:read(1)
      --love.graphics.print(data ,90, 100)

      -- file:close()
      -- use the data .

      -- love.graphics.print(data ,90, 100)
      --if data == "Worked" then
      --love.graphics.print("shit happend" ,90, 100)





    end
    
    if (button == 1 and checkOptions) then

      options = true
      menu = false
      buttonclick:play()
    end
    if (button == 1 and checkExit) then
      buttonclick:play()
      love.event.quit()
      
    end
  end
  if (loadSave == true)then

    if (button == 1 and checkExit) then
      menu = true
      loadSave = false
      buttonclick:play()
    end

  end
  if (options == true)then
    if (button == 1 and checkNormal) then 
       BadguySpeed = 0.5
      buttonclick:play()
    end
    if (button == 1 and checkHard) then 
      
       BadguySpeed = 1
       buttonclick:play()
    end
    

    if (button == 1 and checkExit) then
      menu = true
      options = false
      buttonclick:play()
    end

  end

end
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
  x2 < x1+w1 and
  y1 < y2+h2 and
  y2 < y1+h1
end
function pointInRectangle(pointx, pointy, rectx, recty, rectwidth, rectheight)

  wx = rectx + rectwidth
  hx = recty + rectheight
  return pointx > rectx and pointy > recty and pointx < wx and pointy < hx
end
